from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from rest_framework.test import APITestCase
from rest_framework.test import APIRequestFactory

from rest_framework import status
from .models import *


class CricketTest(APITestCase):

    def get_teams(self):
        # First check for the default behavior
        response = self.client.get('/api/v1/teams/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def get_team_by_slug(self, slug):
        response = self.client.get('/api/v1/teams/' + slug + "/", {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def get_players(self):
        response = self.client.get('/api/v1/player/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def get_players_by_slug(self, slug):
        response = self.client.get('/api/v1/player/' + slug + "/", {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def apply_fixtures(self):
        response = self.client.post('/api/v1/fixtures/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def get_matches(self):
        response = self.client.get('/api/v1/fixtures/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)

    def get_points(self, slug=None):
        if slug is not None:
            response = self.client.get('/api/v1/points/' + slug + "/", {}, format='json')
        else:
            response = self.client.get('/api/v1/points/', {}, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(response.data)
