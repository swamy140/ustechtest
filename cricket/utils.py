from .models import *
import random
from django.db.models import Max


class ExceptionLoggingMiddleware(object):
    """Exception class tracing class"""

    def process_exception(self, request=None, exception=None):
        import traceback
        # print traceback.format_exc()
        return traceback.format_exc()


class LogMessage:
    """log message"""

    def get_log_string(self, message, info=None):
        log = message
        if info:
            user_status = ''
            if info.context.user.is_authenticated:
                user_status = 'User: ' + str(info.context.user.id)
            log = "{0} || Session: {1} || Path: {2} || {3}".format(user_status, self.get_session_key(info.context),
                                                                   info.context.path,
                                                                   message)
        return log

    def get_session_key(self, request):
        return request.session._session_key


from django.conf import settings


class PlayerUtils:
    @staticmethod
    def _get_url(image):
        return settings.BASE_URL + image.url

    @staticmethod
    def json_data(player):
        history = PlayerHistory.objects.get(player=player)
        data = {
            "first_name": player.first_name,
            "last_name": player.last_name,
            "team": {
                'team': player.team.name,
                'club_state': player.team.club_state,
                'logoURI': str(PlayerUtils()._get_url(player.team.logoURI))
            },
            "player_jersey_number": player.player_jersey_number,
            "country": player.country,
            "image": str(PlayerUtils()._get_url(player.image_URI)),
            "player_history": {
                "matches": history.matches,
                'runs': history.runs,
                'highest_score': history.highest_score,
                'fifties': history.fifties,
                'hundreds': history.hundreds
            }
        }
        return data

    def serialize_to_json(cls, players):
        data = list(map(cls.json_data, players))
        return data


from django.db.models import Q


class MatchFixtures:
    def __init__(self):
        self.all_teams = Team.objects.filter(isDeleted=False)

    def get_random_team(self):
        max_id = self.all_teams.aggregate(max_id=Max("id"))['max_id']
        pk = random.randint(1, max_id)
        return Team.objects.get(pk=pk)

    def __exclude_team(self, team):
        self.all_teams = Team.objects.filter(~Q(id=team.pk), isDeleted=False)

    def __fixtures(self):
        team_one = self.get_random_team()
        self.__exclude_team(team_one)
        team_two = self.get_random_team()
        return [team_one, team_two]

    def winning_team(self, teams):
        winner_team = random.choice(teams)
        return winner_team

    def match_fixtures(self):
        teams = self.__fixtures()
        winning_team = self.winning_team(teams)
        lose_team = teams[0] if teams[0] is not winning_team else teams[1]
        matches, _ = Matches.objects.get_or_create(team_one=teams[0], team_two=teams[1], winner_of_match=winning_team)
        self.create_or_update_points(winning_team, lose_team)

        return matches

    def create_or_update_points(self, winning_team, lose_team):
        match_winning_team_points = 5
        if winning_team:
            try:
                points = Points.objects.get(team=winning_team)
                points.matches += 1
                points.points += match_winning_team_points
                points.won += 1
                points.save()
            except:
                Points.objects.create(team=winning_team, matches=1, won=1, lost=0, tied=0,
                                      points=match_winning_team_points)
        if lose_team:
            try:
                points = Points.objects.get(team=lose_team)
                points.matches += 1
                points.lost += 1
                points.save()
            except:
                Points.objects.create(team=lose_team, matches=1, won=0, lost=1, tied=0, points=0)
