from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import JsonResponse
from .serializer import *
from rest_framework import status
from .utils import *
import logging

logger = logging.getLogger(__name__)


# Create your views here.

class TeamAPI(APIView):
    """this Class is used for crate the team and get the list of teams"""

    def post(self, request):
        """this method is used to save the team details"""
        team_serializer = TeamSerializer(data=request.data)
        if team_serializer.is_valid(raise_exception=ValueError):
            team = Team(name=request.data.get("name"),club_state=request.data.get("club_state"),logoURI=request.FILES['logoURI'])
            team.save()
            if team:
                article_data = {'slug': team.slug, 'status': 'CREATED'}
                return JsonResponse(article_data)
            return JsonResponse({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse(team_serializer.errors)

    def put(self, request, slug):
        """this method is used to update the team based on slug"""
        try:
            team = Team.objects.filter(slug=slug)
            if team:
                ream_serializer = TeamSerializer(data=request.data)
                if ream_serializer.is_valid(raise_exception=ValueError):
                    is_success, team = ream_serializer.update(team, request.data)
                    if is_success:
                        team_data = {'slug': team.slug, 'status': 'CREATED'}
                        return JsonResponse(team_data)
                    return JsonResponse({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)

            return Response(data={'error': 'player not found'}, status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request, slug=None):
        """this method to fetch the team related matches"""
        try:
            if slug is not None:
                matches = Matches.objects.filter(Q(team_one__slug=slug) or Q(team_two__slug=slug))

                if matches:
                    data = MatchSerializer(matches, many=True)
                    return Response(data=data.data, status=status.HTTP_200_OK)
                return Response(data=[], status=status.HTTP_200_OK)
            else:
                teams = TeamSerializer(Team.objects.filter(isDeleted=False).order_by('name'), many=True)
                return Response(teams.data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class TeamPlayersBySlugAPI(APIView):
    """this class is used for get the team based on slug value"""

    def get(self, request, slug):
        try:
            players = Player.objects.filter(isDeleted=False, team__slug=slug).order_by('first_name')
            if players:
                players_list = PlayerUtils().serialize_to_json(players)
                return Response(players_list, status=status.HTTP_200_OK)
            return Response(data=[], status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PlayersAPI(APIView):
    """this class are used to perform the three operations such as POST,GET,PUT"""
    @staticmethod
    def get_data_player(data,file,team):
        return {
            'first_name':data.get('first_name'),
            'last_name': data.get('last_name'),
            'country': data.get('country'),
            'image_URI': file,
            'player_jersey_number':data.get('player_jersey_number'),
            'team':team,

        }

    @staticmethod
    def get_data_player_history(data,intance):
        return {
            'matches': data.get('matches'),
            'runs': data.get('runs'),
            'highest_score': data.get('highest_score'),
            'fifties': data.get('fifties'),
            'hundreds': data.get('hundreds'),
            'player':intance

        }
    def post(self, request):
        """this method is used to save the player details"""
        try:

            player_serializer = PlayerSerializer(data=request.data)
            if player_serializer.is_valid(raise_exception=ValueError):
                file = request.FILES['imageURL']
                try:
                    team = Team.objects.get(slug=request.data.get('team'))
                except:
                    return Response(data={'error': 'Team not found'}, status=status.HTTP_404_NOT_FOUND)
                player_instance = Player.objects.create(**self.get_data_player(request.data,file,team))
                history = self.get_data_player_history(request.data,player_instance)

                player = PlayerHistory.objects.create(**history)

                if player and player_instance:
                    player_data = {'slug': player_instance.slug, 'status': 'CREATED'}
                    return JsonResponse(player_data)
                return JsonResponse({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)
            return JsonResponse(player_serializer.errors)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(player_serializer.errors, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def put(self, request, slug):
        """this method is used to update the player details"""
        try:
            player = Player.objects.filter(slug=slug)
            if player:
                player_serializer = PlayerSerializer(data=request.data)
                if player_serializer.is_valid(raise_exception=ValueError):
                    is_success, player = player_serializer.update_player(player, request.data)
                    if is_success:
                        player_data = {'slug': player.slug, 'status': 'CREATED'}
                        return JsonResponse(player_data)
                    return JsonResponse({'error': 'Bad Request'}, status=status.HTTP_400_BAD_REQUEST)

            return Response(data={'error': 'player not found'}, status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        """this method is used to fetch the all players details"""
        try:
            players = Player.objects.filter(isDeleted=False).order_by('first_name')
            if players:
                players_list = PlayerUtils().serialize_to_json(players)
                return Response(data=players_list, status=status.HTTP_200_OK)
            return Response(data=[], status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PlayerBySlugAPI(APIView):
    """this class are used to perform the GET method"""

    def get(self, request, slug):
        """this method is used to fetch the player details based on slug(its unique field)"""
        try:
            players = Player.objects.filter(slug=slug, isDeleted=False)
            players_list = list()
            if players:
                players_list = PlayerUtils().serialize_to_json(players)
            return Response(data=players_list, status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class MatchesAPI(APIView):
    """this class are used to perform the two basic operation such as POST and GET"""

    def post(self, request):
        """this method is used to do the Match fixtures randomly and save the winning team """
        try:
            matches = MatchFixtures().match_fixtures()
            data = MatchSerializer(matches)
            return Response(data=data.data, status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        """this method to get fetch the all matches """
        try:
            all_matches = Matches.objects.all()

            if all_matches:
                data = MatchSerializer(all_matches, many=True)
                return Response(data=data.data, status=status.HTTP_200_OK)
            return Response(data=[], status=status.HTTP_200_OK)

        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class PointsTableAPI(APIView):
    """this Class based views are used to get the all team points list and team based points"""

    def get(self, request, slug=None):
        try:

            if slug is not None:
                points = Points.objects.filter(team__slug=slug)
            else:
                points = Points.objects.all()

            if points:
                data = PointsSerializer(points, many=True)
                return Response(data=data.data, status=status.HTTP_200_OK)
            return Response(data=[],status=status.HTTP_200_OK)
        except Exception as e:
            logger.critical(ExceptionLoggingMiddleware().process_exception(request, e))
            return Response(data={'error': 'Internal Server error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
