from django.db import models
from django.utils.text import slugify

from django.utils.translation import ugettext as _


# Create your models here.


# class Country(models.Model):
#     """this model for Country"""
#     name = models.CharField(max_length=30, default=None, unique=True)
#     slug = models.SlugField(unique=True)
#
#     def __str__(self):
#         return self.name
#
#     class Meta:
#         verbose_name = 'Country'
#         verbose_name_plural = 'Countries'
#
#     def _get_unique_slug(self):
#         slug = slugify(self.name + self.club_state, allow_unicode=True)
#         unique_slug = slug
#         num = 1
#         while Team.objects.filter(slug=unique_slug).exists():
#             unique_slug = '{}-{}'.format(slug, num)
#             num += 1
#         return unique_slug
#
#     def save(self, *args, **kwargs):
#         if not self.slug:
#             self.slug = self._get_unique_slug()
#         super().save(*args, **kwargs)

class CustomModel(models.Model):
    createdAt = models.DateTimeField(auto_now_add=True)
    isDeleted = models.BooleanField(default=False)

    @classmethod
    def get_field_names(cls):
        field_names = list()
        for field in CustomModel._meta.fields:
            field_names.append(field.attname)
        return field_names

    class Meta:
        abstract = True


class Team(CustomModel):
    """this model for Teams"""
    name = models.CharField(max_length=200, default=None, unique=True)
    slug = models.SlugField(unique=True)
    logoURI = models.ImageField(upload_to='media/')
    club_state = models.CharField(max_length=100)

    def __str__(self):
        return '{} - {}'.format(self.name, self.club_state)

    class Meta:
        verbose_name = 'Team'
        verbose_name_plural = 'Teams'

    def _get_unique_slug(self):
        slug = slugify(self.name + self.club_state, allow_unicode=True)
        unique_slug = slug
        num = 1
        while Team.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug


    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)


class Player(CustomModel):
    """this model for Player details"""
    first_name = models.CharField(max_length=100, default=None)
    last_name = models.CharField(max_length=100, default=None)
    slug = models.SlugField(unique=True)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='teams')
    image_URI = models.ImageField(upload_to='media/')
    player_jersey_number = models.IntegerField(unique=True)
    country = models.CharField(
        max_length=50,
        default=None)  # ForeignKey(Country, on_delete=models.CASCADE, related_name='country')  # county is foreignkey

    def __str__(self):
        return '{} {} {}'.format(self.first_name, self.last_name, self.player_jersey_number)

    # def __unicode__(self):
    #     return '%d: %s' % (self.first_name, self.last_name)

    class Meta:
        verbose_name = 'Player'
        verbose_name_plural = 'Players'

    def _get_unique_slug(self):
        slug = slugify(self.first_name + self.last_name + '_' + str(self.player_jersey_number), allow_unicode=True)
        unique_slug = slug
        num = 1
        while Player.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)


class PlayerHistory(CustomModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE, related_name='player')  # player in foreignkey
    matches = models.IntegerField()
    runs = models.IntegerField()
    highest_score = models.IntegerField()
    fifties = models.IntegerField()
    hundreds = models.IntegerField()

    def __str__(self):
        return '{} {}'.format(self.player.first_name, self.player.last_name)

    class Meta:
        verbose_name = 'Player History'
        verbose_name_plural = 'Player History'


class Matches(CustomModel):
    team_one = models.ForeignKey(Team, related_name='first_inning', on_delete=models.CASCADE)
    team_two = models.ForeignKey(Team, related_name='second_inning', on_delete=models.CASCADE)
    winner_of_match = models.ForeignKey(Team, related_name='winner', on_delete=models.CASCADE, blank=True)

    def __str__(self):
        return self.team_one.name

    class Meta:
        verbose_name = 'Matches'
        verbose_name_plural = 'Matches'


class Points(CustomModel):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    matches = models.IntegerField()
    won = models.IntegerField()
    lost = models.IntegerField()
    tied = models.IntegerField()
    points = models.IntegerField()

    def __str__(self):
        return self.team.name

    class Meta:
        verbose_name = 'Points'
        verbose_name_plural = 'Points'
