from django.contrib import admin
from .models import *


# Register your models here.
# admin.site.register(Country)


class TeamAdmin(admin.ModelAdmin):
    """  Team view for admin"""

    list_display = (
        'id', 'name', 'club_state', 'slug', 'logoURI',

    )
    list_per_page = 50
    list_filter = ['name']
    prepopulated_fields = {'slug': ['name', 'club_state']}
    search_fields = ['name']


class PlayerAdmin(admin.ModelAdmin):
    """  Players view for admin"""

    list_display = (
        'id', 'first_name', 'last_name', 'team',

    )
    list_per_page = 50
    list_filter = ['first_name']
    prepopulated_fields = {'slug': ['first_name', 'last_name']}
    search_fields = ['first_name']

    def team(self, instance):
        return instance.team.name


admin.site.register(Team, TeamAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(PlayerHistory)
admin.site.register(Matches)
admin.site.register(Points)
