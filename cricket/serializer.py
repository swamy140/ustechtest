from rest_framework import serializers
from .models import *


# class CountrySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Country
#         fields = ('name',)


class TeamSerializer(serializers.ModelSerializer):
    slug = serializers.CharField(required=False,max_length=230)
    class Meta:
        model = Team
        fields = ('name', 'logoURI', 'club_state', 'slug',)

    # def create(self, validated_data):
    #     try:
    #
    #         team = Team(name=validated_data.pop('name'),club_state=validated_data.pop('club_state'),logoURI=validated_data.pop('logoURI'))
    #         team.save()
    #     except Exception as e:
    #         return False,None
    #     return True, team


class PlayerHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = PlayerHistory
        fields = ('matches', 'runs', 'highest_score', 'fifties', 'hundreds')


class PlayerSerializer(serializers.ModelSerializer):
    team = serializers.CharField(max_length=200, required=True)
    player_details = PlayerHistorySerializer(many=False, required=False)

    class Meta:
        model = Player
        fields = ('first_name', 'last_name', 'team', 'player_jersey_number', 'country', 'player_details',)

    def create(self, validated_data,player_isntance):
        # team = validated_data.pop('team')
        # _ = validated_data.pop("imageURL")
        player_de = validated_data.pop('player_details')

        # team_data = Team.objects.get(slug=team[0])
        # validated_data['team'] = team_data
        # player = Player(**validated_data)
        # player.image_URI = file
        # player.save()
        player_de['player'] = player_isntance
        history, _ = PlayerHistory.objects.get_or_create(**player_de)
        return True, player_isntance

    def update_player(self, instance, validated_data):
        team = validated_data.pop('team')
        player_de = validated_data.pop('player_details')

        team_data = Team.objects.get(name=team)
        validated_data['team'] = team_data
        instance.first_name = validated_data.pop('first_name')
        instance.last_name = validated_data.pop('last_name')
        instance.team = team
        instance.player_jersey_number = validated_data.pop('player_jersey_number')
        instance.country = validated_data.pop('country')
        history_instance = PlayerHistory.objects.filter(player=instance)
        if history_instance:
            history_instance.matches = player_de.pop('matches')
            history_instance.runs = player_de.pop('runs')
            history_instance.highest_score = player_de.pop('highest_score')
            history_instance.fifties = player_de.pop('fifties')
            history_instance.hundreds = player_de.pop('hundreds')
        return True, instance


class MatchSerializer(serializers.ModelSerializer):
    team_one = TeamSerializer()
    team_two = TeamSerializer()
    winner_of_match = TeamSerializer()

    class Meta:
        model = Matches
        fields = ('team_one', 'team_two', 'winner_of_match',)


class PointsSerializer(serializers.ModelSerializer):
    team = TeamSerializer()

    class Meta:
        model = Points
        fields = ('team', 'matches', 'won', 'lost', 'tied', 'points')
