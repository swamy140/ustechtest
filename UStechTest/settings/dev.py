from UStechTest.settings.base import *

DEBUG = True

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        # 'ENGINE': 'django.db.backends.mysql',
        # 'NAME': 'sample',
        # 'HOST': 'localhost',
        # 'PORT': '3306',
    }
}

CORS_ORIGIN_ALLOW_ALL = True