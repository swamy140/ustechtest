"""UStechTest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from cricket.views import *
from django.conf import settings
from django.conf.urls.static import static

version = 'v1'

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path(settings.VERSION.get(version) + 'team/', TeamAPI.as_view()),
                  path(settings.VERSION.get(version) + 'team-between-matches/<slug:slug>/', TeamAPI.as_view()),
                  path(settings.VERSION.get(version) + 'team/<slug:slug>/', TeamPlayersBySlugAPI.as_view()),
                  path(settings.VERSION.get(version) + 'player/', PlayersAPI.as_view()),
                  path(settings.VERSION.get(version) + 'player/<slug:slug>/', PlayerBySlugAPI.as_view()),
                  path(settings.VERSION.get(version) + 'fixtures/', MatchesAPI.as_view()),
                  path(settings.VERSION.get(version) + 'points/', PointsTableAPI.as_view()),
                  path(settings.VERSION.get(version) + 'points/<slug:slug>/', PointsTableAPI.as_view()),
                  # path(settings.VERSION.get(version) + 'winner-team/<slug:team_one>/<slug:team_two>/',
                  #      MatchesAPI.as_view()),

              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
