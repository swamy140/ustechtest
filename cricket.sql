-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: cricket
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add user',4,'add_user'),(14,'Can change user',4,'change_user'),(15,'Can delete user',4,'delete_user'),(16,'Can view user',4,'view_user'),(17,'Can add content type',5,'add_contenttype'),(18,'Can change content type',5,'change_contenttype'),(19,'Can delete content type',5,'delete_contenttype'),(20,'Can view content type',5,'view_contenttype'),(21,'Can add session',6,'add_session'),(22,'Can change session',6,'change_session'),(23,'Can delete session',6,'delete_session'),(24,'Can view session',6,'view_session'),(25,'Can add Matches',7,'add_matches'),(26,'Can change Matches',7,'change_matches'),(27,'Can delete Matches',7,'delete_matches'),(28,'Can view Matches',7,'view_matches'),(29,'Can add Player',8,'add_player'),(30,'Can change Player',8,'change_player'),(31,'Can delete Player',8,'delete_player'),(32,'Can view Player',8,'view_player'),(33,'Can add Player History',9,'add_playerhistory'),(34,'Can change Player History',9,'change_playerhistory'),(35,'Can delete Player History',9,'delete_playerhistory'),(36,'Can view Player History',9,'view_playerhistory'),(37,'Can add Points',10,'add_points'),(38,'Can change Points',10,'change_points'),(39,'Can delete Points',10,'delete_points'),(40,'Can view Points',10,'view_points'),(41,'Can add Team',11,'add_team'),(42,'Can change Team',11,'change_team'),(43,'Can delete Team',11,'delete_team'),(44,'Can view Team',11,'view_team');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$120000$XXNzHI7uCaJS$bLOIbdF81WM+AGdh9rNeMVu6tunKN2akZq5/OBh60Qc=','2019-12-11 13:35:12.189240',1,'admin','','','',1,1,'2019-12-11 12:06:05.800602');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cricket_matches`
--

DROP TABLE IF EXISTS `cricket_matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cricket_matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `team_one_id` int(11) NOT NULL,
  `team_two_id` int(11) NOT NULL,
  `winner_of_match_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cricket_matches_team_one_id_4dc653c8_fk_cricket_team_id` (`team_one_id`),
  KEY `cricket_matches_team_two_id_283ba120_fk_cricket_team_id` (`team_two_id`),
  KEY `cricket_matches_winner_of_match_id_912658b2_fk_cricket_team_id` (`winner_of_match_id`),
  CONSTRAINT `cricket_matches_team_one_id_4dc653c8_fk_cricket_team_id` FOREIGN KEY (`team_one_id`) REFERENCES `cricket_team` (`id`),
  CONSTRAINT `cricket_matches_team_two_id_283ba120_fk_cricket_team_id` FOREIGN KEY (`team_two_id`) REFERENCES `cricket_team` (`id`),
  CONSTRAINT `cricket_matches_winner_of_match_id_912658b2_fk_cricket_team_id` FOREIGN KEY (`winner_of_match_id`) REFERENCES `cricket_team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cricket_matches`
--

LOCK TABLES `cricket_matches` WRITE;
/*!40000 ALTER TABLE `cricket_matches` DISABLE KEYS */;
INSERT INTO `cricket_matches` VALUES (1,'2019-12-11 14:14:44.223319',0,3,3,3),(2,'2019-12-11 15:46:00.040385',0,1,4,4),(3,'2019-12-11 15:46:08.550572',0,7,3,7),(4,'2019-12-11 17:13:47.439037',0,5,2,5),(5,'2019-12-11 17:14:22.702090',0,3,4,3),(6,'2019-12-11 17:14:37.494348',0,1,3,3),(7,'2019-12-11 17:14:40.795711',0,7,3,3),(8,'2019-12-11 17:14:48.128994',0,2,5,5),(9,'2019-12-11 17:14:55.655173',0,3,1,1),(10,'2019-12-11 17:14:59.659334',0,1,4,1),(11,'2019-12-11 17:15:26.922910',0,4,1,4),(12,'2019-12-11 17:15:30.524496',0,5,6,6),(13,'2019-12-11 17:16:25.651508',0,6,2,2),(14,'2019-12-11 17:17:28.068124',0,7,1,7),(15,'2019-12-11 17:17:31.275050',0,5,7,5),(16,'2019-12-11 17:17:34.040117',0,3,2,2),(17,'2019-12-11 17:17:36.862606',0,4,3,3),(18,'2019-12-11 17:34:10.935068',0,4,6,6),(19,'2019-12-11 17:34:18.142288',0,5,6,5);
/*!40000 ALTER TABLE `cricket_matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cricket_player`
--

DROP TABLE IF EXISTS `cricket_player`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cricket_player` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `image_URI` varchar(100) NOT NULL,
  `player_jersey_number` int(11) NOT NULL,
  `country` varchar(50) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `player_jersey_number` (`player_jersey_number`),
  KEY `cricket_player_team_id_f5507f98_fk_cricket_team_id` (`team_id`),
  CONSTRAINT `cricket_player_team_id_f5507f98_fk_cricket_team_id` FOREIGN KEY (`team_id`) REFERENCES `cricket_team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cricket_player`
--

LOCK TABLES `cricket_player` WRITE;
/*!40000 ALTER TABLE `cricket_player` DISABLE KEYS */;
INSERT INTO `cricket_player` VALUES (1,'2019-12-11 13:42:52.482575',0,'Shikhar','Dhawan','shikhar-dhawan','media/ddd.jpg',23,'India',1),(2,'2019-12-11 13:43:55.485381',0,'Shreyas','Iyer','shreyas-iyer','media/Shreyas.jpg',1,'India',1),(3,'2019-12-11 13:44:50.183380',0,'Prithvi','Shaw','prithvi-shaw','media/Shaw.jpg',4,'India',1),(4,'2019-12-11 13:47:53.248463',0,'Rahul','K. L.','rahul-k-l','media/rahul.jpg',13,'India',2),(5,'2019-12-11 13:48:40.243926',0,'Mayank','Agarwal','mayank-agarwal','media/Agarwal.jpg',3,'India',2),(6,'2019-12-11 13:49:58.440649',0,'Nitish Rana','Rana','nitish-rana-rana','media/Rana.jpg',20,'India',3),(7,'2019-12-11 13:50:54.230380',0,'Rinku Singh','Singh','rinku-singh-singh','media/Rinku_Singh.jpg',40,'India',3),(8,'2019-12-11 13:52:00.929994',0,'Anmolpreet','Singh','anmolpreet-singh','media/Anmolpreet.jpg',18,'India',4),(9,'2019-12-11 13:52:37.465875',0,'Rohit Sharma','Rohit Sharma','rohit-sharma-rohit-sharma','media/Rohit_Sharma.jpg',14,'India',4),(10,'2019-12-11 13:53:24.217959',0,'Schin','Tendulkar','schin-tendulkar','media/sachin.jpg',10,'India',4),(11,'2019-12-11 13:54:26.218719',0,'Manan','Vohra','manan-vohra','media/Manan.jpg',24,'India',7),(12,'2019-12-11 13:55:06.561449',0,'Shashank Singh','Shashank Singh','shashank-singh-shashank-singh','media/Shashank_Singh.jpg',29,'India',7),(13,'2019-12-11 13:55:58.000957',0,'Virat Kohli','Kohli','virat-kohli-kohli','media/Virat_Kohli.jpg',9,'India',5),(14,'2019-12-11 13:56:39.358306',0,'Devdutt Padikkal','Devdutt Padikkal','devdutt-padikkal-devdutt-padikkal','media/Devdutt_Padikkal.jpg',7,'India',5),(15,'2019-12-11 13:57:37.117261',0,'Manish Pandey','Manish Pandey','manish-pandey-manish-pandey','media/Manish_Pandey.jpg',5,'India',6),(16,'2019-12-11 13:58:19.672836',0,'Shreevats Goswami','Shreevats Goswami','shreevats-goswami-shreevats-goswami','media/Shreevats_Goswami.jpg',11,'India',6);
/*!40000 ALTER TABLE `cricket_player` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cricket_playerhistory`
--

DROP TABLE IF EXISTS `cricket_playerhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cricket_playerhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `matches` int(11) NOT NULL,
  `runs` int(11) NOT NULL,
  `highest_score` int(11) NOT NULL,
  `fifties` int(11) NOT NULL,
  `hundreds` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cricket_playerhistory_player_id_b443f5e3_fk_cricket_player_id` (`player_id`),
  CONSTRAINT `cricket_playerhistory_player_id_b443f5e3_fk_cricket_player_id` FOREIGN KEY (`player_id`) REFERENCES `cricket_player` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cricket_playerhistory`
--

LOCK TABLES `cricket_playerhistory` WRITE;
/*!40000 ALTER TABLE `cricket_playerhistory` DISABLE KEYS */;
INSERT INTO `cricket_playerhistory` VALUES (1,'2019-12-11 13:45:24.263550',0,200,2000,140,30,20,1),(2,'2019-12-11 13:45:48.565312',0,300,20000,200,20,30,2),(3,'2019-12-11 13:46:09.739998',0,100,14000,150,30,10,3),(7,'2019-12-11 13:59:40.821291',0,300,12000,150,34,32,4),(8,'2019-12-11 13:59:59.276420',0,130,10000,130,30,13,5),(9,'2019-12-11 14:00:17.536046',0,120,16000,130,30,20,6),(10,'2019-12-11 14:00:29.734917',0,200,12000,200,40,20,7),(11,'2019-12-11 14:00:48.404987',0,200,120,130,24,13,8),(12,'2019-12-11 14:01:04.120953',0,200,1400,140,20,40,9),(13,'2019-12-11 14:01:35.108909',0,320,22000,230,89,70,10),(14,'2019-12-11 14:01:51.849233',0,200,10000,200,100,20,11),(15,'2019-12-11 14:02:13.057917',0,200,20000,120,30,32,12),(16,'2019-12-11 14:02:37.105826',0,200,12000,230,39,30,13),(17,'2019-12-11 14:02:54.285495',0,200,20000,240,40,23,14),(18,'2019-12-11 14:03:10.604099',0,200,13000,145,30,20,15),(19,'2019-12-11 14:03:26.858081',0,200,20000,230,43,24,16);
/*!40000 ALTER TABLE `cricket_playerhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cricket_points`
--

DROP TABLE IF EXISTS `cricket_points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cricket_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `matches` int(11) NOT NULL,
  `won` int(11) NOT NULL,
  `lost` int(11) NOT NULL,
  `tied` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cricket_points_team_id_504adb15_fk_cricket_team_id` (`team_id`),
  CONSTRAINT `cricket_points_team_id_504adb15_fk_cricket_team_id` FOREIGN KEY (`team_id`) REFERENCES `cricket_team` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cricket_points`
--

LOCK TABLES `cricket_points` WRITE;
/*!40000 ALTER TABLE `cricket_points` DISABLE KEYS */;
INSERT INTO `cricket_points` VALUES (1,'2019-12-11 14:14:44.228532',0,12,8,4,0,40,3),(2,'2019-12-11 15:46:00.057966',0,8,2,6,0,10,4),(3,'2019-12-11 15:46:00.077357',0,6,2,4,0,10,1),(4,'2019-12-11 15:46:08.555588',0,5,2,3,0,10,7),(5,'2019-12-11 17:13:47.464211',0,6,5,1,0,25,5),(6,'2019-12-11 17:13:47.474294',0,5,2,3,0,10,2),(7,'2019-12-11 17:15:30.528554',0,4,2,2,0,10,6);
/*!40000 ALTER TABLE `cricket_points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cricket_team`
--

DROP TABLE IF EXISTS `cricket_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cricket_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `logoURI` varchar(100) NOT NULL,
  `club_state` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cricket_team`
--

LOCK TABLES `cricket_team` WRITE;
/*!40000 ALTER TABLE `cricket_team` DISABLE KEYS */;
INSERT INTO `cricket_team` VALUES (1,'2019-12-11 13:37:04.750607',0,'Delhi Capitals','delhi-capitals-delhi','media/delhi.png','Delhi'),(2,'2019-12-11 13:37:54.271659',0,'Kings XI Punjab','kings-xi-punjab-punjab','media/pung.jpg','Punjab'),(3,'2019-12-11 13:38:35.043756',0,'Kolkata Knight Riders','kolkata-knight-riders-kolkata','media/kolkta.png','Kolkata'),(4,'2019-12-11 13:39:17.434663',0,'Mumbai Indians','mumbai-indians-mumbai','media/mubai.jpg','Mumbai'),(5,'2019-12-11 13:40:05.117082',0,'Royal Challengers','royal-challengers-bangalore','media/royal.png','Bangalore'),(6,'2019-12-11 13:40:44.630459',0,'Sunrisers','sunrisers-hyderabad','media/sun.png','Hyderabad'),(7,'2019-12-11 13:41:21.435001',0,'Rajasthan Royals','rajasthan-royals-rajasthan','media/rajastan.png','Rajasthan');
/*!40000 ALTER TABLE `cricket_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-12-11 13:37:04.752786','1','Delhi Capitals - Delhi',1,'[{\"added\": {}}]',11,1),(2,'2019-12-11 13:37:55.446290','2','Kings XI Punjab - Punjab',1,'[{\"added\": {}}]',11,1),(3,'2019-12-11 13:38:35.044600','3','Kolkata Knight Riders - Kolkata',1,'[{\"added\": {}}]',11,1),(4,'2019-12-11 13:39:17.437848','4','Mumbai Indians - Mumbai',1,'[{\"added\": {}}]',11,1),(5,'2019-12-11 13:40:05.118308','5','Royal Challengers - Bangalore',1,'[{\"added\": {}}]',11,1),(6,'2019-12-11 13:40:44.633579','6','Sunrisers - Hyderabad',1,'[{\"added\": {}}]',11,1),(7,'2019-12-11 13:41:21.435771','7','Rajasthan Royals - Rajasthan',1,'[{\"added\": {}}]',11,1),(8,'2019-12-11 13:42:52.484305','1','Shikhar Dhawan 23',1,'[{\"added\": {}}]',8,1),(9,'2019-12-11 13:43:55.488116','2','Shreyas Iyer 1',1,'[{\"added\": {}}]',8,1),(10,'2019-12-11 13:44:50.185043','3','Prithvi Shaw 4',1,'[{\"added\": {}}]',8,1),(11,'2019-12-11 13:45:24.264895','1','Shikhar Dhawan',1,'[{\"added\": {}}]',9,1),(12,'2019-12-11 13:45:48.567740','2','Shreyas Iyer',1,'[{\"added\": {}}]',9,1),(13,'2019-12-11 13:46:09.740509','3','Prithvi Shaw',1,'[{\"added\": {}}]',9,1),(14,'2019-12-11 13:47:53.251780','4','Rahul K. L. 13',1,'[{\"added\": {}}]',8,1),(15,'2019-12-11 13:48:40.244973','5','Mayank Agarwal 3',1,'[{\"added\": {}}]',8,1),(16,'2019-12-11 13:49:58.443925','6','Nitish Rana Rana 20',1,'[{\"added\": {}}]',8,1),(17,'2019-12-11 13:50:54.231194','7','Rinku Singh Singh 40',1,'[{\"added\": {}}]',8,1),(18,'2019-12-11 13:52:00.933281','8','Anmolpreet Singh 18',1,'[{\"added\": {}}]',8,1),(19,'2019-12-11 13:52:37.466703','9','Rohit Sharma Rohit Sharma 14',1,'[{\"added\": {}}]',8,1),(20,'2019-12-11 13:53:24.218825','10','Schin Tendulkar 10',1,'[{\"added\": {}}]',8,1),(21,'2019-12-11 13:54:26.221957','11','Manan Vohra 24',1,'[{\"added\": {}}]',8,1),(22,'2019-12-11 13:55:06.562218','12','Shashank Singh Shashank Singh 29',1,'[{\"added\": {}}]',8,1),(23,'2019-12-11 13:55:58.001928','13','Virat Kohli Kohli 9',1,'[{\"added\": {}}]',8,1),(24,'2019-12-11 13:56:39.361636','14','Devdutt Padikkal Devdutt Padikkal 7',1,'[{\"added\": {}}]',8,1),(25,'2019-12-11 13:57:37.118121','15','Manish Pandey Manish Pandey 5',1,'[{\"added\": {}}]',8,1),(26,'2019-12-11 13:58:19.673714','16','Shreevats Goswami Shreevats Goswami 11',1,'[{\"added\": {}}]',8,1),(27,'2019-12-11 13:58:47.744649','4','Shikhar Dhawan',1,'[{\"added\": {}}]',9,1),(28,'2019-12-11 13:58:47.874612','5','Shikhar Dhawan',1,'[{\"added\": {}}]',9,1),(29,'2019-12-11 13:59:00.418321','5','Shreyas Iyer',2,'[{\"changed\": {\"fields\": [\"player\"]}}]',9,1),(30,'2019-12-11 13:59:18.543374','6','Prithvi Shaw',1,'[{\"added\": {}}]',9,1),(31,'2019-12-11 13:59:40.824224','7','Rahul K. L.',1,'[{\"added\": {}}]',9,1),(32,'2019-12-11 13:59:59.276907','8','Mayank Agarwal',1,'[{\"added\": {}}]',9,1),(33,'2019-12-11 14:00:17.538973','9','Nitish Rana Rana',1,'[{\"added\": {}}]',9,1),(34,'2019-12-11 14:00:29.735428','10','Rinku Singh Singh',1,'[{\"added\": {}}]',9,1),(35,'2019-12-11 14:00:48.405571','11','Anmolpreet Singh',1,'[{\"added\": {}}]',9,1),(36,'2019-12-11 14:01:04.123848','12','Rohit Sharma Rohit Sharma',1,'[{\"added\": {}}]',9,1),(37,'2019-12-11 14:01:35.109459','13','Schin Tendulkar',1,'[{\"added\": {}}]',9,1),(38,'2019-12-11 14:01:51.849786','14','Manan Vohra',1,'[{\"added\": {}}]',9,1),(39,'2019-12-11 14:02:13.060855','15','Shashank Singh Shashank Singh',1,'[{\"added\": {}}]',9,1),(40,'2019-12-11 14:02:37.106397','16','Virat Kohli Kohli',1,'[{\"added\": {}}]',9,1),(41,'2019-12-11 14:02:54.286046','17','Devdutt Padikkal Devdutt Padikkal',1,'[{\"added\": {}}]',9,1),(42,'2019-12-11 14:03:10.607032','18','Manish Pandey Manish Pandey',1,'[{\"added\": {}}]',9,1),(43,'2019-12-11 14:03:26.858579','19','Shreevats Goswami Shreevats Goswami',1,'[{\"added\": {}}]',9,1),(44,'2019-12-11 14:19:04.009563','5','Shreyas Iyer',3,'',9,1),(45,'2019-12-11 14:22:55.235566','6','Prithvi Shaw',3,'',9,1),(46,'2019-12-11 15:41:33.199210','4','Shikhar Dhawan',3,'',9,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(7,'cricket','matches'),(8,'cricket','player'),(9,'cricket','playerhistory'),(10,'cricket','points'),(11,'cricket','team'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-11 12:05:22.616041'),(2,'auth','0001_initial','2019-12-11 12:05:24.732255'),(3,'admin','0001_initial','2019-12-11 12:05:25.211211'),(4,'admin','0002_logentry_remove_auto_add','2019-12-11 12:05:25.229117'),(5,'admin','0003_logentry_add_action_flag_choices','2019-12-11 12:05:25.239221'),(6,'contenttypes','0002_remove_content_type_name','2019-12-11 12:05:25.516913'),(7,'auth','0002_alter_permission_name_max_length','2019-12-11 12:05:25.547919'),(8,'auth','0003_alter_user_email_max_length','2019-12-11 12:05:25.579665'),(9,'auth','0004_alter_user_username_opts','2019-12-11 12:05:25.598864'),(10,'auth','0005_alter_user_last_login_null','2019-12-11 12:05:25.738141'),(11,'auth','0006_require_contenttypes_0002','2019-12-11 12:05:25.748603'),(12,'auth','0007_alter_validators_add_error_messages','2019-12-11 12:05:25.766100'),(13,'auth','0008_alter_user_username_max_length','2019-12-11 12:05:25.790753'),(14,'auth','0009_alter_user_last_name_max_length','2019-12-11 12:05:25.822355'),(15,'cricket','0001_initial','2019-12-11 12:05:28.087576'),(16,'sessions','0001_initial','2019-12-11 12:05:28.235438');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('1yx0xcnhmix4kt738c3v9t18bygsl0yl','M2ZmMTliMzVjYzE3N2NhZDkxNmRkM2M5ODVjZjliZTNhYmU3N2U2NDp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJhOGMyOGEzMTE3YzhiNzZkNDQ4YmYyN2I5MDkxNTVmNTNiZDA0YWEzIn0=','2019-12-25 13:35:12.199480');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-11 17:51:11
